// import dayjs from 'dayjs'
// import 'dayjs/locale/zh-cn'
import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
Vue.use(uView);
// dayjs.locale('zh-cn')
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
