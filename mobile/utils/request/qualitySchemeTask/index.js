import request from '../index.js'


export const basedataQualitySchemeGetAll = (params) => {
    return request('/basedataQualityScheme/getAll', params)
}


export const qualitySchemeTaskCreateMaterialTask = (params) => {
    return request('/qualitySchemeTask/createMaterialTask', params)
}

export const qualitySchemeTaskCreateWorkOrderTask = (params) => {
    return request('/qualitySchemeTask/createWorkOrderTask', params)
}


