import request from '../index.js'


// 登录
export const Login = (params) => {
    return request('/basedataAuth/login', params)
}

// 退出登录
export const Logout = (params) => {
    return request('/basedataAuth/signOut', params)
}
