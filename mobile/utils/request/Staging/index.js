import request from '../index.js'


// 查询列表
export const getList = (params) => {
	return request('/basedataEquipment/queryByWorkOrder', params)
}

// 查询详情
export const getDetails = (params) => {
	return request('/workOrder/queryByWorkId', params)
}

// 查询图表详情
export const padWorkDetail = (params) => {
	return request('/workOrder/pdaWorkDetail', params)
}




// 点击完成

export const updateStatus = (params) => {
	return request('/workOrder/updateAndproceStatus', params, {
		method: 'put'
	})
}

export const updateWorkOrderStatus = (params) => {
	return request('/workOrder/update', params, {
		method: 'put'
	})
}

export const qualitySchemeTask = (params) => {
	return request('/qualitySchemeTask/createWorkOrderTask', params, {
		method: 'post'
	})
}


export const workOrderStartRecord = (params) => {
	return request('/workOrderStartRecord/save', params, {
		method: 'post'
	})
}
export const putMaterialRedord = (params) => {
	return request('/workOrderStartRecord/putMaterialRedord', params, {
		method: 'post'
	})
}

export const queryIndexOrder = (params) => {
	return request('/workOrder/queryIndexOrder', params, {
		method: 'post'
	})
}


// 查询质检模版
export const getBasedataProcedureResource = (params) => {
	return request('/basedataProcedureResource/getPage', params, {
		method: 'post'
	})
}


export const getWarehouseStorageMaterial = (workId) => {
	return request('/throwMaterialRecord/queryMaterialList?workId='+workId, {}, {
		method: 'get'
	})
}

export const getWarehouseStorageMaterialPageByPojo = (params) => {
	return request('/warehouseStorageMaterial/pageByPojo',params)
}

export const queryWorkBatchNo = (params) => {
	return request('/reportWorkRecord/queryWorkBatchNo',params)
}

export const saveBatch = (params) => {
	return request('/reportWorkRecord/saveReportData',params)
}

// 查询班组


export const getBasedataTeam = (params) => {
	return request('/basedataTeam/getPage',params)
}

export const getBadRange = (params) => {
	return request('/qualityBadCode/getPage',params)
}

export const getToolRegisterCode = (params) => {
	return request('/reportWorkRecord/getToolRegisterCode',params)
}

// 查询班次
export const getSwingShift = (params) => {
	return request('/swingShift/getPage',params)
}

// 查询用户

export const getBasedataUser = (params) => {
	return request('/basedataUser/getPage',params)
}

export const reportWorkRecordQueryResidueCount = (params) => {
	return request('/reportWorkRecord/queryResidueCount',params)
}

export const warehouseStorageMaterialQueryByWarehouseId = (params) => {
	return request('/warehouseStorageMaterial/queryByWarehouseId',params)
}

export const queryIsWorkorderOrProcedure = (params) => {
	return request('/workOrder/queryIsWorkorderOrProcedure',params)
}









