const defaults = {
	method: 'POST',
	loading: true
}

// 根据环境配置baseURL
const getBaseUrl = () => {
	const ip = uni.getStorageSync("ip") || '172.30.190.11:9016'
	console.log(ip);
	let protocol = 'http://';
	
	// 检查IP地址是否以特定的IP结尾
	if (ip.endsWith('172.30.190.56:9012')) {
	  protocol = 'https://';
	}
	
	const baseUrl = `${protocol}${ip}/api`;
	console.log(baseUrl,'baseUrl');
	return baseUrl
}

export default (path, data = {}, config = defaults, header = {}) => {
	const token = uni.getStorageSync('token')
	const userIdS = uni.getStorageSync('userId')
	const Authorization = token ? `${token}` : undefined
	const userId = userIdS ? userIdS : ''
	if (config.loading) {
		uni.showLoading({
			title: '加载中',
			mask: true
		})
	}
	return new Promise((resolve, reject) => {
		uni.request({
			header: {
				"BearerToken": Authorization,
				"Authorization":Authorization,
				"userId": userId,
				'Content-type': 'application/json',
				...header
			},
			sslVerify:false,
			url: getBaseUrl() + path,
			method: config.method,
			data,
			success(response) {
				const {
					code,
					message,
					error
				} = response.data
				if (code === 200) {
					resolve(response.data)
				} else {
					if ( response.data.status === 401) {
						handleUnauthorized()
					}
					console.log(response.data)
					showErrorModal(message || error)
					reject(response.data)

				}
			},
			fail(err) {
				// if (err.code === 401) {
				// 	handleUnauthorized()
				// }
				uni.showToast({
					icon: 'none',
					title: '服务响应失败'
				})
				reject(err)
			},
			complete() {
				uni.hideLoading()
			}
		})
	})
}

function showErrorModal(message) {
	uni.showModal({
		content: message ? message : "服务器内部错误",
		duration: 3000,
		showCancel: false
	})
}

function handleUnauthorized() {
	uni.clearStorage() // 清除缓存
	uni.reLaunch({
		url: '/pages/Login/login'
	})
}