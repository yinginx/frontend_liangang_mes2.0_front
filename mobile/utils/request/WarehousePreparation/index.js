import request from '../index.js'


// 查询
export const getList = (params) => {
	return request('/warehouseMaterialTransferOrder/listByPojoPage', params)
}
// 
export const getPlanProcedureOrderAll = (params) => {
	return request('/planProcedureOrder/getAll', params)
}
export const shipmentPlanDetail = (params) => {
	return request('/shipmentPlanDetail/getAll', params)
}
export const awaitShipmentsMaterial = (params) => {
	return request('/awaitShipmentsMaterial/save', params)
}

export const warehouseStorageMaterialpPageByPojo = (params) => {
	return request('/warehouseStorageMaterial/pageByPojo', params)
}

export const warehouseStorageMaterialQueryByMaterialWarehouse = (params) => {
	return request('/warehouseStorageMaterial/queryByMaterialWarehouse', params)
}

// 查询物料详情


export const getDetailById = (params) => {
	return request('/warehouseMaterialTransferOrder/getDetailById', params)
}

export const basedataStorageGetAll = (params) => {
	return request('/basedataStorage/getAll', params)
}

export const outStock = (params) => {
	return request('/warehouseMaterialTransferOrder/outStock', params)
}
export const pageByPojo = (params) => {
	return request('/warehouseStorageMaterial/pageByPojo', params)
}

export const warehouseMaterialTransferOrderSave = (params) => {
	return request('/warehouseMaterialTransferOrder/outStockPad', params)
}

export const saveOutStockBatch = (params) => {
	return request('/warehouseMaterialTransferOrder/outStockBatch', params)
}

export const warehouseMaterialTransferOrderOutStockStorage = (params) => {
	return request('/warehouseMaterialTransferOrder/outStockStorage', params)
}


export const warehouseMaterialTransferTaskpageByPojo = (params) => {
	return request('/warehouseMaterialTransferTask/pageByPojo', params)
}


export const warehouseMaterialTransferOrderInStock = (params) => {
	return request('/warehouseMaterialTransferOrder/inStock', params)
}

export const warehouseMaterialTransferOrderReject = (params) => {
	return request('/warehouseMaterialTransferOrder/reject', params)
}

export const warehouseStorageMaterialUpdateMaterialStatus	 = (params) => {
	return request('/warehouseStorageMaterial/updateMaterialStatus	', params)
}

export const warehouseMateriaITransferOrderListByPojoPage = (params) => {
	return request('/warehouseMaterialTransferOrder/listByPojoPage', params)
}

export const warehouseMateriaITransferOrderGetDetailByld = (params) => {
	return request('/warehouseMaterialTransferOrder/getDetailById', params)
}
export const warehouseMaterITransferOrderInStock = (params) => {
	return request('/warehouseMaterITransferOrder/inStock', params)
}

export const basedataStorageAll = (params) => {
	return request('/basedataStorage/getAll', params)
}

export const basedataQualityScheme = (params) => {
	return request('/basedataQualityScheme/getPage', params)
}


export const shipmentPlanGetPage = (params) => {
	return request('/shipmentPlanDetail/getPage', params)
}


