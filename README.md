## Node =>18.20.3




## 基于 Vue 3, TypeScript, Element Plus
为了简化代码中的导入语句，使用了 `unplugin-auto-import` 和 `unplugin-vue-components` 插件来实现自动按需导入。

## 部分功能仍要按需引入

```html
<el-icon :size="size" :color="color">
 <Edit />
</el-icon>

import {Edit} from '@element-plus/icons-vue'
```

```javascript
import {ElMessage, ElMessageBox} from 'element-plus'
```







