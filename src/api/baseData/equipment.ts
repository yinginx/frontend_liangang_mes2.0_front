import request from "@/utils/http/api";
export const equipmentPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataEquipment/getPage',
        data: params
    })
}

export const equipmentAdd = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataEquipment/save',
        data: params
    })
}


export const equipmentUpdate = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataEquipment/update',
        data: params
    })
}



export const equipmentDel= (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataEquipment/delete',
        data: params
    })
}

