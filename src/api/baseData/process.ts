// 工序表
import request from "@/utils/http/api";
export const editProcedureApi = (params: any) => {
  return request({
    method: "put",
    url: "/api/basedataProcedureResource/update",
    data: params,
  });
};
export const addProcedureApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataProcedureResource/save",
    data: params,
  });
};
export const deleteProcedureApi = (params: any) => {
  return request({
    method: "delete",
    url: "/api/basedataProcedureResource/delete",
    data: params,
  });
};
export const productProcedureApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataProcedureResource/getPage",
    data: params,
  });
};
