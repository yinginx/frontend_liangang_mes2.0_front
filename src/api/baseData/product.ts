import request from "@/utils/http/api";

export const materialPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataMaterial/getPage',
        data: params
    })
}
//产品
export const productPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProduct/getPage',
        data: params
    })
}

export const productAdd = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProduct/save',
        data: params
    })
}


export const productUpdate = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataProduct/update',
        data: params
    })
}



export const productDel= (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataProduct/deleteByIds',
        data: params
    })
}

