import request from "@/utils/http/api";
//物料bom
export const MaterialBomPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataMaterialBom/getPage',
        data: params
    })
}

export const MaterialBomAdd = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataMaterialBom/save',
        data: params
    })
}


export const MaterialBomUpdate = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataMaterialBom/update',
        data: params
    })
}



export const MaterialBomDel= (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataMaterialBom/delete',
        data: params
    })
}

//产品
export const productAll = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProduct/getAll',
        data: params
    })
}

export const materialAllApi = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataMaterial/getAll',
        data: params
    })
}
//物料bom关联编辑
export const editApi = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataMaterialBomDetails/update',
        data: params
    })
}
//物料bom关联新建
export const addApi = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataMaterialBomDetails/save',
        data: params
    })
}
// 删除
export const deleteApi = (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataMaterialBomDetails/delete',
        data: params
    })
}
//物料bom关联查询
export const getPageApi = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataMaterialBomDetails/getPage',
        data: params
    })
}
