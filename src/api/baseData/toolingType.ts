import request from "@/utils/http/api";
//工装
export const toolPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataTooling/getPage',
        data: params
    })
}

export const toolAdd = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataTooling/save',
        data: params
    })
}


export const toolUpdate = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataTooling/update',
        data: params
    })
}



export const toolDel= (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataTooling/delete',
        data: params
    })
}

