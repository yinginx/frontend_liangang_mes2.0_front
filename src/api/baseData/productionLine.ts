import request from "@/utils/http/api";
//产线
export const lineAll = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataOrganization/getPage',
        data: params
    })
}
export const addLine = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataOrganization/save',
        data: params
    })
}

export const editLine = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataOrganization/update',
        data: params
    })
}

export const deltLine = (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataOrganization/delete',
        data: params
    })
}