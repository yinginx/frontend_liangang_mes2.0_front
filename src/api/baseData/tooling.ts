import request from "@/utils/http/api";
//工装
export const toolPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/toolRegister/pageByParam',
        data: params
    })
}

export const toolAdd = (params:any) => {
    return request({
        method: 'post',
        url: '/api/toolRegister/save',
        data: params
    })
}


export const toolUpdate = (params:any) => {
    return request({
        method: 'put',
        url: '/api/toolRegister/update',
        data: params
    })
}


export const toolDel= (params:any) => {
    return request({
        method: 'delete',
        url: '/api/toolRegister/delete',
        data: params
    })
}

