import request from "@/utils/http/api";

export const materialPage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProduct/getAll',
        data: params
    })
}
//产品
export const routePage = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProcessRoute/listProcesssRoute',
        data: params
    })
}

export const routeAdd = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProcessRoute/save',
        data: params
    })
}


export const routeUpdate = (params:any) => {
    return request({
        method: 'put',
        url: '/api/basedataProcessRoute/update',
        data: params
    })
}



export const routeDel= (params:any) => {
    return request({
        method: 'delete',
        url: '/api/basedataProcessRoute/delete',
        data: params
    })
}

export const routeCopy = (params:any) => {
    return request({
        method: 'post',
        url: '/api/basedataProcessRoute/copy',
        data: params
    })
}