import request from "@/utils/http/api"; //编辑

// 工序订单表查询
export const procedureOrderDataApi = (data: any) => {
  return request({
    method: "post",
    url: "/api/planProcedureOrder/getPage",
    data: data,
  });
};

//产品表
export const productAllApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataProduct/getAll",
    data: params,
  });
};

//删除
export const delProcedureOrderApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/workOrder/delete",
    data: params,
  });
};

//查找工序配置
export const procedureResourceApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataProcedureResource/getPage",
    data: params,
  });
};

//物料
export const materialApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataMaterial/getAll",
    data: params,
  });
};

//测试库位
export const basedataStorageApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataStorage/getAll",
    data: params,
  });
};

//生成领料单详情
export const materialTransferOrderApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/planProcedureOrder/getMaterialRequisition",
    data: params,
  });
};

//设备
export const equipmentAll = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataEquipment/getAll",
    data: params,
  });
};

// 投入物料
export const putMaterialRedord = (params: any) => {
  return request({
    method: "post",
    url: "/api/workOrder/saveBatch",
    data: params,
  });
};

//开工
export const basedataWorkRecord = (params: any) => {
  return request({
    method: "post",
    url: "/api/workOrderStartRecord/save",
    data: params,
  });
};

//开工
export const procedureOrder = (params: any) => {
  return request({
    method: "post",
    url: "/api/planProcedureOrder/procedureOrderQueryprocedure",
    data: params,
  });
};

//下发
export const basedataWorkOrderUpdate = (params: any) => {
  return request({
    method: "post",
    url: "/api/workOrder/updateStatus",
    data: params,
  });
};

export const delBasedataWorkOrder = (params: any) => {
  return request({
    method: "post",
    url: "/api/workOrder/delete",
    data: params,
  });
};
//工序下的工装

export const queryToolingByWork = (params: any) => {
  return request({
    method: "post",
    url: "/api/planProcedureOrder/queryToolingByWork",
    data: params,
  });
};

export const queryEquipment = (params: any) => {
  return request({
    method: "post",
    url: "/api/planProcedureOrder/queryEquipment",
    data: params,
  });
};
