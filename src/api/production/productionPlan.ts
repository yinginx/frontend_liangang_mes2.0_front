//生产计划
import request from "@/utils/http/api"; //编辑
export const editBaseDataPlanApi = (params: any) => {
  return request({
    method: "put",
    url: "/api/planOrder/update",
    data: params,
  });
};
//新建
export const addBaseDataPlanApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/planOrder/save",
    data: params,
  });
};
// 删除
export const deleteBaseDataPlanApi = (params: any) => {
  return request({
    method: "delete",
    url: "/api/planOrder/delete",
    data: params,
  });
};
//查询
export const productBaseDataPlanApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/planOrder/getPage",
    data: params,
  });
};

//查询分页条件
export const productBaseDataPlanApiQuery = (params: any) => {
  return request({
    method: "post",
    url: "/api/planOrder/listByQuery",
    data: params,
  });
};

export const ProductionPlanDetailApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/planDetail/getPage",
    data: params,
  });
};

export const productAll = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataProduct/getAll",
    data: params,
  });
};

export const lineAll = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataOrganization/getPage",
    data: params,
  });
};
//工艺路线

export const allRouteApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataProcessRoute/getPage",
    data: params,
  });
};

//物料bom查询
export const materialBomApi = (params: any) => {
  return request({
    method: "post",
    url: "/api/basedataMaterialBom/getPage",
    data: params,
  });
};
//下发

export const issuePlan = (params: any) => {
  return request({
    method: "post",
    url: "/api/planDetail/issuePlan",
    data: params,
  });
};

export const queryByPlanIdDetail = (params: any) => {
  return request({
    method: "post",
    url: "/api/planOrder/queryByPlanIdDetail",
    data: params,
  });
};
