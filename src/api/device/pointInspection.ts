import request from "@/utils/http/api";
//
export const equipmentInspectionTaskPage = (params: any) => {
  return request({
    method: "post",
    url: "/api/equipmentInspectionTask/getPage",
    data: params,
  });
};

export const equipmentInspectionTaskAdd = (params: any) => {
  return request({
    method: "post",
    url: "/api/equipmentInspectionTask/save",
    data: params,
  });
};

export const equipmentInspectionTaskUpdate = (params: any) => {
  return request({
    method: "put",
    url: "/api/equipmentInspectionTask/update",
    data: params,
  });
};

export const equipmentInspectionTaskDel = (params: any) => {
  return request({
    method: "delete",
    url: "/api/equipmentInspectionTask/delete",
    data: params,
  });
};


export const cycleSettingAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/cycleSetting/getAll",
      data: params,
    });
  };
  

  //设备
  
  export const equipmentAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/basedataEquipment/getAll",
      data: params,
    });
  };
  