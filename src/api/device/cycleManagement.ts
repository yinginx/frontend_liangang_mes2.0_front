import request from "@/utils/http/api";
//物料
export const materialPage = (params: any) => {
  return request({
    method: "post",
    url: "/api/cycleSetting/getPage",
    data: params,
  });
};

export const materialAdd = (params: any) => {
  return request({
    method: "post",
    url: "/api/cycleSetting/save",
    data: params,
  });
};

export const materialUpdate = (params: any) => {
  return request({
    method: "put",
    url: "/api/cycleSetting/update",
    data: params,
  });
};

export const materialDel = (params: any) => {
  return request({
    method: "delete",
    url: "/api/cycleSetting/delete",
    data: params,
  });
};
