import request from "@/utils/http/api";
//物料
export const equipmentInspectionPlanPage = (params: any) => {
  return request({
    method: "post",
    url: "/api/equipmentInspectionPlan/getPage",
    data: params,
  });
};

export const equipmentInspectionPlanAdd = (params: any) => {
  return request({
    method: "post",
    url: "/api/equipmentInspectionPlan/save",
    data: params,
  });
};

export const equipmentInspectionPlanUpdate = (params: any) => {
  return request({
    method: "put",
    url: "/api/equipmentInspectionPlan/update",
    data: params,
  });
};

export const equipmentInspectionPlanDel = (params: any) => {
  return request({
    method: "delete",
    url: "/api/equipmentInspectionPlan/delete",
    data: params,
  });
};


export const cycleSettingAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/cycleSetting/getAll",
      data: params,
    });
  };
  

  //设备
  
  export const equipmentAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/basedataEquipment/getAll",
      data: params,
    });
  };
  


  
  //工装
  
  export const toolRegisterAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/toolRegister/getAll",
      data: params,
    });
  };
  

    //人员管理
  
    export const userPersonAll = (params: any) => {
      return request({
        method: "post",
        url: "/api/basedataUser/getAll",
        data: params,
      });
    };
    