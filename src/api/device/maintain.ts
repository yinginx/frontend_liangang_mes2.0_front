import request from "@/utils/http/api";
//维修记录
export const maintenanceOrderPage = (params: any) => {
  return request({
    method: "post",
    url: "/api/maintenanceOrder/getPage",
    data: params,
  });
};

export const maintenanceOrderAdd = (params: any) => {
  return request({
    method: "post",
    url: "/api/maintenanceOrder/save",
    data: params,
  });
};

export const maintenanceOrderUpdate = (params: any) => {
  return request({
    method: "put",
    url: "/api/maintenanceOrder/update",
    data: params,
  });
};

export const maintenanceOrderDel = (params: any) => {
  return request({
    method: "delete",
    url: "/api/maintenanceOrder/delete",
    data: params,
  });
};
  //设备
  
  export const equipmentAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/basedataEquipment/getAll",
      data: params,
    });
  };
  

  export const maintenanceMalfunctionAll = (params: any) => {
    return request({
      method: "post",
      url: "/api/maintenanceMalfunction/getAll",
      data: params,
    });
  };
  