import request from "@/utils/http/api";
//
export const maintenanceMalfunctionPage = (params: any) => {
  return request({
    method: "post",
    url: "/api/maintenanceMalfunction/getPage",
    data: params,
  });
};

export const maintenanceMalfunctionAdd = (params: any) => {
  return request({
    method: "post",
    url: "/api/maintenanceMalfunction/save",
    data: params,
  });
};

export const maintenanceMalfunctionUpdate = (params: any) => {
  return request({
    method: "put",
    url: "/api/maintenanceMalfunction/update",
    data: params,
  });
};

export const maintenanceMalfunctionDel = (params: any) => {
  return request({
    method: "delete",
    url: "/api/maintenanceMalfunction/delete",
    data: params,
  });
};
