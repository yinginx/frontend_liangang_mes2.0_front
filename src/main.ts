
import zhCN from "element-plus/dist/locale/zh-cn.mjs"; //引入中文
import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import './assets/main.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'  
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';
import 'element-plus/dist/index.css';
import { printClick } from './globalMethods.ts'; // 引入全局方法
import './assets/css/global.css' // 引入全局样式
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {  
    app.component(key, component)  
  }
app.use(createPinia())
app.use(router)
app.use(ElementPlus, { locale: zhCN })



app.config.globalProperties.$printClick = printClick; // 注册为全局方法

app.mount('#app')
