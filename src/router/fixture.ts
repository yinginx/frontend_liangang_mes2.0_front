import Menu from '@/components/menu/MenuIndex.vue';

export default [
	{
		path: '/fixture',
		title: '工装管理',
		name: 'fixture',
		sort: 30,
		component: Menu,
		redirect: '/fixture/cycleManagement',
		children: [
			{
				path: '/fixture/cycleManagement',
				title: '周期管理',
				name: 'fixtureCycleManagement',
				component: () => import('@/views/fixture/cycleManagement/index.vue')
			},
			{
				path: '/fixture/creatPlan',
				title: '工装计划',
				name: 'fixtureCreatPlan',
				component: () => import('@/views/fixture/creatPlan/index.vue')
			},
			{
				path: '/fixture/pointInspection',
				title: '点检管理',
				name: 'fixturePointInspection',
				component: () => import('@/views/fixture/pointInspection/index.vue')
			},
			{
				path: '/fixture/spotCheck',
				title: '巡检管理',
				name: 'fixtureSpotCheck',
				component: () => import('@/views/fixture/spotCheck/index.vue')
			},
			{
				path: '/fixture/maintenance',
				title: '保养任务',
				name: 'fixtureMaintenance',
				component: () => import('@/views/fixture/maintenance/index.vue')
			},
			{
				path: '/fixture/faultType',
				title: '故障类型',
				name: 'fixtureFaultType',
				component: () => import('@/views/fixture/faultType/index.vue')
			},
			{
				path: '/fixture/maintain',
				title: '维修记录',
				name: 'fixtureMaintain',
				component: () => import('@/views/fixture/maintain/index.vue')
			}


		]
	}
];