import Menu from '../components/menu/MenuIndex.vue';

export default [
	{
		path: '/setting',
		name: 'setting',
		title: '系统设置',
		component: Menu,
		redirect: '/setting/user',
		sort: 50,
		children: [
			{
				path: '/setting/person',
				name: 'person',
				title: '人员管理',
				component: () => import('@/views/baseData/person/index.vue')
			},
			{
				path: '/setting/user',
				name: 'user',
				title: '用户管理',
				component: () => import('@/views/baseData/user/index.vue')
			},
			{
				path: '/setting/role',
				name: 'role',
				title: '权限设置',
				component: () => import('@/views/setting/role.vue')
			},
			{
				path: '/setting/traceabilityCode',
				title: '模号取值位数维护',
				name: 'traceabilityCode',
				component: () => import('@/views/baseData/traceabilityCode/index.vue')
			},
			{
				path: '/setting/stoCodeRules',
				title: '出入库转换规则维护',
				name: 'stoCodeRules',
				component: () => import('@/views/baseData/stoCodeRules/index.vue')
			}
		]
	}
];
