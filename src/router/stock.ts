import Menu from '../components/menu/MenuIndex.vue';

export default [
	{
		path: '/stock',
		name: 'stock',
		title: '仓储管理',
		component: Menu,
		redirect: '/stock/index',
		sort: 25,
		children: [
			{
				path: '/stock/index',
				title: '仓库位管理',
				name: 'stock',
				component: () => import('@/views/stock/stock/index.vue')
			},
			{
				path: '/stock/materialIn/index',
				title: '入库单',
				name: 'materialIn',
				component: () => import('@/views/stock/materialIn/index.vue')
			},
			{
				path: '/stock/materialOut/index',
				title: '出库单',
				name: 'materialOut',
				component: () => import('@/views/stock/materialOut/index.vue')
			},
			{
				path: '/stock/materialTransfer/index',
				title: '调拨单',
				name: 'materialTransfer',
				component: () => import('@/views/stock/materialTransfer/index.vue')
			},
			{
				path: '/stock/materialStock/index',
				title: '备料单',
				name: 'materialStock',
				component: () => import('@/views/stock/materialStock/index.vue')
			},
			{
				path: '/stock/materiaInTask/index',
				title: '入库任务',
				name: 'materiaInTask',
				component: () => import('@/views/stock/materiaInTask/index.vue')
			},
			{
				path: '/stock/taskList/index',
				title: '任务记录',
				name: 'taskList',
				component: () => import('@/views/stock/taskList/index.vue')
			},
			{
				path: '/stock/inventoryDetail/index',
				title: '库存详情',
				name: 'inventoryDetail',
				component: () => import('@/views/stock/inventoryDetail/index.vue')
			},
			{
				path: '/stock/inventoryDetail/repertoryTransfer',
				title: '库存调整记录',
				name: 'repertoryTransfer',
				component: () => import('@/views/stock/inventoryDetail/repertoryTransfer.vue')
			},
			{
				path: '/stock/deliverySchedule/index',
				title: '发货计划',
				name: 'deliverySchedule',
				component: () => import('@/views/stock/deliverySchedule/index.vue')
			},
			{
				path: '/stock/stocktaking/index',
				title: '盘点',
				name: 'stocktaking',
				component: () => import('@/views/stock/stocktaking/index.vue')
			},
			{
				path: '/stock/stocktaking/report',
				title: '盘点记录',
				name: 'stocktakingReport',
				component: () => import('@/views/stock/stocktaking/report.vue')
			},
			{
				path: '/stock/materialExamine/index',
				title: '二次报检',
				name: 'materialExamine',
				component: () => import('@/views/stock/materialExamine/index.vue')
			},
			{
				path: '/stock/moveStockReport/index',
				title: '移库记录报表',
				name: 'moveStockReport',
				component: () => import('@/views/stock/moveStockReport/index.vue')
			},
			{
				path: '/stock/quickOutboundReport/index',
				title: '快速出库记录',
				name: 'quickOutboundReport',
				component: () => import('@/views/stock/quickOutboundReport/index.vue')
			}
		]
	}
];
