import Menu from '@/components/menu/MenuIndex.vue';

export default [
  {
    path: '/production',
    title: '计划管理',
    name: 'production',
    component: Menu,
    sort: 5,
    redirect: '/production/production',
    children: [
      // {
      //   path: "/production/productionPlan",
      //   name: "生产计划",
      //   component: () => import("../views/production/productionPlan/index.vue"),
      // },
      {
        path: '/production/production',
        title: '生产计划',
        name: 'production',
        component: () => import('@/views/production/production/index.vue')
      },
      {
        path: '/production/productionAdd',
        title: '增改计划信息',
        noMenu: true,
        name: 'productionAdd',
        component: () => import('@/views/production/production/productionAdd.vue')
      },
      {
        path: '/production/productionDetail',
        title: '生产计划详情',
        name: 'productionDetail',
        component: () => import('@/views/production/productionDetail/index.vue')
      },
      {
        path: '/production/rawMaterialDemand',
        title: '原材料需求统计',
        name: 'rawMaterialDemand',
        component: () => import('@/views/production/rawMaterialDemand/index.vue')
      }
    ]
  }
];
