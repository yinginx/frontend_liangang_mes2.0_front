import Menu from '../components/menu/MenuIndex.vue';

export default [
    {
        path: '/scheduling',
        title: '生产执行',
        name: 'scheduling',
        sort: 10,
        component: Menu,
        redirect: '/scheduling/dispatchManagement',
        children: [
            {
                path: '/scheduling/dispatchManagement',
                title: '派工管理',
                name: 'dispatchManagement',
                component: () => import('@/views/scheduling/dispatchManagement/index.vue')
            },

            {
                path: '/scheduling/manufacturingExec',
                title: '生产执行首页',
                name: 'manufacturingExec',
                component: () => import('@/views/scheduling/manufacturingExec/index.vue')
            },
            {
                path: '/scheduling/workPlanDetail',
                title: '生产执行详情',
                name: 'workPlanDetail',
                noMenu: true,
                component: () => import('@/views/scheduling/manufacturingExec/workPlanDetail.vue')
            },
            {
                path: '/scheduling/workJobReport',
                title: '报工详情',
                name: 'workJobReport',
                noMenu: true,
                component: () => import('@/views/scheduling/manufacturingExec/workJobReport.vue')
            },
            {
                path: '/scheduling/workPlanFeeding',
                title: '投料详情',
                name: 'workPlanFeeding',
                noMenu: true,
                component: () => import('@/views/scheduling/manufacturingExec/workPlanFeeding.vue')
            },
            {
                path: '/scheduling/toolEquipment',
                title: '工装更换',
                name: 'toolEquipment',
                component: () => import('@/views/scheduling/manufacturingExec/toolEquipment.vue')
            },
            {
                path: '/scheduling/materialStockScheduling',
                title: '领料单管理',
                name: 'materialStockScheduling',
                component: () => import('@/views/scheduling/materialStockScheduling/index.vue')
            },
            {
                path: '/scheduling/dispatchManagementDetail',
                title: '派工详情',
                name: 'dispatchManagementDetail',
                noMenu: true,
                component: () => import('@/views/scheduling/dispatchManagementDetail/index.vue')
            },
            {
                path: '/scheduling/rawMaterialsRecords',
                title: '投料消耗查询',
                name: 'rawMaterialsRecords',
                component: () => import('@/views/scheduling/rawMaterialsRecords/index.vue')
            },
            {
                path: '/scheduling/workReportTecord',
                title: '报工记录表',
                name: 'schedulingReportTecord',
                component: () => import('@/views/report/produceReport/workReportTecord.vue')
            },
            {
                path: '/scheduling/equipmentPerformanceManage',
                title: '设备绩效管理',
                name: 'equipmentPerformanceManage',
                children: [{
                    path: '/scheduling/equipmentLiveStatus',
                    title: '设备实时状态',
                    name: 'equipmentLiveStatus',
                    component: () => import('@/views/scheduling/equipmentPerformanceManage/equipmentLiveStatus.vue')
                }, {
                    path: '/scheduling/equipmentLiveStatusdetail',
                    title: '设备状态详情',
                    name: 'equipmentLiveStatusdetail',
                    component: () => import('@/views/scheduling/equipmentPerformanceManage/equipmentLiveStatusdetail.vue')
                }, {
                    path: '/scheduling/alarmDetails',
                    title: '报警明细',
                    name: 'alarmDetails',
                    component: () => import('@/views/scheduling/equipmentPerformanceManage/alarmDetails.vue')
                }]
            },
            {
                path: '/scheduling/temporaryPrint',
                title: '临时打印',
                name: 'temporaryPrint',
                component: () => import('@/views/report/produceReport/temporaryPrint.vue')
            },
        ]
    },
];
