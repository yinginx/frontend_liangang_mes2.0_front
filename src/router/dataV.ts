import Menu from '@/components/menu/MenuIndex.vue';

export default [
	{
		path: '/dataV',
		title: '智能看板',
		name: 'dataV',
		sort: 60,
		component: Menu,
		redirect: '/dataV/index',
		children: [
			{
				path: '/dataV/index',
				title: '看板列表',
				name: 'dataIndex',
				component: () => import('@/views/dataV/index.vue')
			}
		]
	}
];