import { createRouter, createWebHistory } from 'vue-router';
import base from './base';
import outSourcing from './outSourcing';
import quality from './quality';
import setting from './setting';
import stock from './stock';
import scheduling from './scheduling';
import device from './device';
import production from './production';
import report from './report';
import fixture from './fixture';
import dataV from './dataV';

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			redirect: '/home'
		},
		{
			path: '/home',
			name: 'index',
			title: '首页',
			component: () => import('@/views/home/index.vue')
		},
		{
			path: '/home',
			redirect: '/home/login',
			name: 'home',
			title: '登录',
			children: [
				{
					path: '/home/login',
					name: 'indexPage',
					title: '登录',
					component: () => import('@/views/loginPage/index.vue')
				}
			]
		},
		...base,
		...stock,
		...quality,
		...outSourcing,
		...setting,
		...scheduling,
		...device,
		...fixture,
		...production,
		...report,
		...dataV,
		// {
		// 	path: '/loginPage',
		// 	name: 'loginPage',
		// 	title: '登录',
		// 	noMenu: true,
		// 	component: () => import('@/views/loginPage/index.vue') // 注意这里要用箭头函数
		// },
		{
			path: '/indexPrint',
			name: 'indexPrint',
			title: '打印',
			noMenu: true,
			component: () => import('@/views/404/indexPrint.vue') // 注意这里要用箭头函数
		},
		// 通配符路由应该放在最后
		{
			path: '/:pathMatch(.*)*',
			name: 'NotFound',
			title: '404',
			component: () => import('@/views/404/index.vue'),
			noMenu: true
		}
	]
});

// 全局前置守卫
router.beforeEach((to, from, next) => {
	if (to.path !== '/home/login') {
		const token = localStorage.getItem('token');
		if (!token) {
			router.push('/home/login');
		}
	}
	// console.log("Navigating to:", to.path)
	// if (to.path === moldManagement[0].children[0].path) {
	// 	// 恢复菜单为第一项
	// 	localStorage.setItem('defaultActiveString', '0-0-0');
	// }
	next();
});

export default router;
