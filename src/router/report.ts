import Menu from '@/components/menu/MenuIndex.vue';

export default [
	{
		path: '/report',
		title: 'KPI管理',
		name: 'report',
		component: Menu,
		sort: 40,
		redirect: '/report/OEEReport',
		children: [
			{
				path: '/report/OEEReportTeam',
				title: 'OEE报表',
				name: 'OEEReportTeam',
				children: [{
					path: '/report/OEEReport',
					title: 'OEE绩效',
					name: 'OEEReport',
					component: () => import('@/views/report/OEEReport.vue')
				}, {
					path: '/report/OEEIndex',
					title: 'OEE详情',
					name: 'OEEIndex',
					component: () => import('@/views/report/OEEReport/index.vue')
				}]
			},
			{
				path: '/report/planReportTeam',
				title: '计划报表',
				name: 'planReportTeam',
				children: [{
					path: '/report/planReport',
					title: '计划进度报表',
					name: 'planReport',
					component: () => import('@/views/report/planReport/index.vue')
				}, {
					path: '/report/planReportDetail',
					title: '计划详情报表',
					name: 'planReportDetail',
					component: () => import('@/views/production/productionDetail/index.vue')
				}, {
					path: '/report/detailsOfWorkstation',
					title: '工站库存详情',
					name: 'detailsOfWorkstation',
					component: () => import('@/views/report/planReport/detailsOfWorkstation.vue')
				}
				]
			},
			{
				path: '/report/produceReport',
				title: '生产报表',
				name: 'produceReport',
				children: [
					{
						path: '/report/workOrderProgress',
						title: '工单进度',
						name: 'workOrderProgress',
						component: () => import('@/views/report/produceReport/workOrderProgress.vue')
					}, {
						path: '/report/workReportTecord',
						title: '报工记录表',
						name: 'workReportTecord',
						component: () => import('@/views/report/produceReport/workReportTecord.vue')
					}, {
						path: '/report/dailyProduction',
						title: '生产日报表',
						name: 'dailyProduction',
						component: () => import('@/views/report/produceReport/dailyProduction.vue')
					}, {
						path: '/report/dailyWorkshop',
						title: '车间日报表',
						name: 'dailyWorkshop',
						component: () => import('@/views/report/produceReport/dailyWorkshop.vue')
					},
					{
						path: '/report/throwReport',
						title: '投料记录报表',
						name: 'throwReport',
						component: () => import('@/views/report/produceReport/throwReport.vue')
					},
					{
						path: '/report/personnelOutput',
						title: '人员产量汇总',
						name: 'personnelOutput',
						component: () => import('@/views/report/produceReport/personnelOutput.vue')
					}]
				// {
				//   path: '/report/productionStatistics',
				//   title: '产量统计查询',
				//   name: 'productionStatistics',
				//   component: () => import('@/views/report/produceReport/productionStatistics.vue'),
				// },
			},
			{
				path: '/report/qualityReport',
				title: '质量报表',
				name: 'qualityReport',
				children: [{
					path: '/report/qualityResQuery',
					title: '质检结果查询',
					name: 'qualityResQuery',
					component: () => import('@/views/report/qualityReport/qualityResQuery.vue')
				}, {
					path: '/report/defectiveProduct',
					title: '不合格品报表',
					name: 'defectiveProduct',
					component: () => import('@/views/report/qualityReport/defectiveProduct.vue')
				}, {
					path: '/report/qualityTask',
					title: '质检任务统计',
					name: 'qualityTask',
					component: () => import('@/views/report/qualityReport/qualityTask.vue')
				}, {
					path: '/report/qualityComprehensive',
					title: '质检任务记录',
					name: 'qualityComprehensive',
					component: () => import('@/views/report/qualityReport/qualityComprehensive.vue')
				}, {
					path: '/report/traceabilityReport',
					title: '正反向追溯',
					name: 'traceabilityReport',
					component: () => import('@/views/report/qualityReport/traceabilityReport.vue')
				},
				{
					path: '/report/qualityComprehensiveNew/index',
					title: '质量综合查询',
					name: 'qualityComprehensiveNew',
					component: () => import('@/views/quality/qualityComprehensiveNew/index.vue')
				}
					//  {
					//   path: '/report/qualityInsAnalysis',
					//   title: '质检效率分析',
					//   name: 'qualityInsAnalysis',
					//   component: () => import('@/views/report/qualityReport/qualityInsAnalysis.vue'),
					// }
				]
			},
			{
				path: '/report/storageReport',
				title: '仓储报表',
				name: 'storageReport',
				children: [{
					path: '/report/materialReport',
					title: '物料库存详情',
					name: 'materialReport',
					component: () => import('@/views/report/storageReport/materialReport.vue')
				}, {
					path: '/report/storageIndex',
					title: '库位库存详情',
					name: 'storageIndex',
					component: () => import('@/views/report/storageReport/storageIndex.vue')
				}, {
					path: '/report/rawMaterial',
					title: '原材料入库',
					name: 'rawMaterial',
					component: () => import('@/views/report/storageReport/rawMaterial.vue')
				}, {
					path: '/report/finishedProduct',
					title: '成品发货报表',
					name: 'finishedProduct',
					component: () => import('@/views/report/storageReport/finishedProduct.vue')
				}, {
					path: '/report/inboundOutbound',
					title: '出入库记录',
					name: 'inboundOutbound',
					component: () => import('@/views/report/storageReport/inboundOutbound.vue')
				}, {
					path: '/report/outReport',
					title: '委外报表',
					name: 'outReport',
					component: () => import('@/views/report/storageReport/outReport.vue')
				}]
			}
		]
	}
];
