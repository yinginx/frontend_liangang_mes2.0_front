import Menu from '@/components/menu/MenuIndex.vue';

export default [
	{
		path: '/base',
		name: 'base',
		title: '基础数据',
		component: Menu,
		redirect: '/base/equipment',
		sort: 45,
		children: [
			{
				path: '/base/equipment',
				name: 'equipment',
				title: '设备管理',
				component: () => import('@/views/baseData/equipment/index.vue')
			},
			{
				path: '/base/toolingType',
				name: 'toolingType',
				title: '工装类型',

				component: () => import('@/views/baseData/toolingType/index.vue')
			},
			{
				path: '/base/tooling',
				name: 'tooling',
				title: '工装台账',
				component: () => import('@/views/baseData/tooling/index.vue')
			},
			{
				path: '/base/materialMaintenance',
				title: '物料管理',
				name: 'materialMaintenance',
				component: () => import('@/views/baseData/materialMaintenance/index.vue')
			},
			{
				path: '/base/product',
				title: '产品管理',
				name: 'product',
				component: () => import('@/views/baseData/product/index.vue')
			},
			{
				path: '/base/productBOM',
				title: '物料BOM',
				name: 'productBOM',
				component: () => import('@/views/baseData/productBOM/index.vue')
			},
			{
				path: '/base/processRoute',
				title: '工艺路线',
				name: 'processRoute',
				component: () => import('@/views/baseData/processRoute/index.vue')
			},
			{
				// path: '/base/processRoute/config/:id',
				path: '/base/processRoute/config',
				title: '资源配置',
				name: 'processRouteConfig',
				component: () => import('@/views/baseData/processRoute/components/resource/index.vue'),
				noMenu: true
			},
			{
				path: '/base/productionLine',
				title: '产线管理',
				name: 'productionLine',
				component: () => import('@/views/baseData/productionLine/index.vue')
			},

			{
				path: '/base/team',
				title: '班组管理',
				name: 'team',
				component: () => import('@/views/baseData/team/index.vue')
			},
			{
				path: '/base/teamClasses',
				title: '班次管理',
				name: 'teamClasses',
				component: () => import('@/views/baseData/team/teamClasses.vue')
			}, {
				path: '/base/transactionSource',
				title: '事务来源',
				name: 'transactionSource',
				component: () => import('@/views/baseData/transactionSource/index.vue')
			},
			{
				path: '/base/customer',
				title: '客户管理',
				name: 'customer',
				component: () => import('@/views/baseData/customer/index.vue')
			},
			{
				path: '/base/customerMaterial',
				title: '物料客户关系',
				name: 'customerMaterial',
				component: () => import('@/views/baseData/customerMaterial/index.vue')
			},
			{
				path: '/base/supplier',
				title: '供应商管理',
				name: 'supplier',
				component: () => import('@/views/baseData/supplier/index.vue')
			},
			{
				path: '/base/supplierMaterial',
				title: '物料供应商关系',
				name: 'supplierMaterial',
				component: () => import('@/views/baseData/supplierMaterial/index.vue')
			},
			{
				path: '/base/factoryCalendar',
				title: '工厂日历设置',
				name: 'factoryCalendar',
				component: () => import('@/views/baseData/factoryCalendar/index.vue')
			}
		]
	}
];
