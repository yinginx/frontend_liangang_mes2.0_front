import Menu from '@/components/menu/MenuIndex.vue';

export default [
	{
		path: '/device',
		title: '设备设置',
		name: 'device',
		sort: 35,
		component: Menu,
		redirect: '/device/cycleManagement',
		children: [
			{
				path: '/device/cycleManagement',
				title: '周期管理',
				name: 'cycleManagement',
				component: () => import('@/views/device/cycleManagement/index.vue')
			},
			{
				path: '/device/creatPlan',
				title: '设备计划',
				name: 'creatPlan',
				component: () => import('@/views/device/creatPlan/index.vue')
			},
			{
				path: '/device/pointInspection',
				title: '点检管理',
				name: 'pointInspection',
				component: () => import('@/views/device/pointInspection/index.vue')
			},
			{
				path: '/device/spotCheck',
				title: '巡检管理',
				name: 'spotCheck',
				component: () => import('@/views/device/spotCheck/index.vue')
			},
			{
				path: '/device/maintenance',
				title: '保养任务',
				name: 'maintenance',
				component: () => import('@/views/device/maintenance/index.vue')
			},
			{
				path: '/device/faultType',
				title: '故障类型',
				name: 'faultType',
				component: () => import('@/views/device/faultType/index.vue')
			},
			{
				path: '/device/maintain',
				title: '维修记录',
				name: 'maintain',
				component: () => import('@/views/device/maintain/index.vue')
			},
			{
				path: '/device/deviceDowntime',
				title: '设备停机',
				name: 'deviceDowntime',
				component: () => import('@/views/device/deviceDowntime/index.vue')
			},
			{
				path: '/device/sparePartStore',
				title: '备件入库',
				name: 'sparePartStore',
				component: () => import('@/views/device/sparePart/sparePartStore.vue')
			},
			{
				path: '/device/sparePartUse',
				title: '备件领用',
				name: 'sparePartUse',
				component: () => import('@/views/device/sparePart/sparePartUse.vue')
			},
			{
				path: '/device/sparePartOutbound',
				title: '备件出库',
				name: 'sparePartOutbound',
				component: () => import('@/views/device/sparePart/sparePartOutbound.vue')
			},
			{
				path: '/device/sparePart',
				title: '备件库存详情',
				name: 'sparePart',
				component: () => import('@/views/device/sparePart/sparePart.vue')
			}
		]
	}
];
