import Menu from '../components/menu/MenuIndex.vue';

export default [
	{
		path: '/quality',
		name: 'quality',
		title: '质量管理',
		component: Menu,
		redirect: '/quality/index',
		sort: 15,
		children: [
			{
				path: '/quality/index',
				title: '质检方案',
				name: 'quality',
				component: () => import('@/views/quality/qualityPlan/index.vue')
			},
			{
				path: '/quality/qualitySchemeTask/index',
				title: '质检任务',
				name: 'qualitySchemeTask',
				component: () => import('@/views/quality/qualitySchemeTask/index.vue')
			},
			{
				path: '/quality/qualityTree/index',
				title: '树形质检任务',
				name: 'qualityTree',
				component: () => import('@/views/quality/qualityTree/index.vue')
			},
			{
				path: '/quality/qualityRecord/index',
				title: '质检记录',
				name: 'qualityRecord',
				component: () => import('@/views/quality/qualityRecord/index.vue')
			},
			{
				path: '/quality/NCR/index',
				title: 'NCR单',
				name: 'NCR',
				component: () => import('@/views/quality/NCR/index.vue')
			},
			{
				path: '/quality/qualityBad/index',
				title: '不合格记录',
				name: 'qualityBad',
				component: () => import('@/views/quality/qualityBad/index.vue')
			},
			{
				path: '/quality/qualityCode/index',
				title: '不良代码定义',
				name: 'qualityCode',
				component: () => import('@/views/quality/qualityCode/index.vue')
			},
		]
	}
];
