import Menu from '../components/menu/MenuIndex.vue';

export default [
	{
		path: '/outSourcing',
		name: 'outSourcing',
		title: '委外管理',
		component: Menu,
		redirect: '/outSourcing/index',
		sort: 20,
		children: [
			{
				path: '/outSourcing/index',
				title: '委外计划',
				name: 'outSourcing',
				component: () => import('@/views/outSourcing/index.vue')
			},
			{
				path: '/outSourcing/outOfstock',
				title: '委外出库',
				name: 'outOfstock',
				component: () => import('@/views/outSourcing/outOfstock/index.vue')
			},
			{
				path: '/outSourcing/inOfstock',
				title: '委外入库',
				name: 'inOfstock',
				component: () => import('@/views/outSourcing/inOfstock/index.vue')
			},
			{
				path: '/outSourcing/outOfBack',
				title: '委外退回',
				name: 'outOfBack',
				component: () => import('@/views/outSourcing/outOfBack/index.vue')
			},
			{
				path: '/outSourcing/inventoryOutReport',
				title: '委外库存',
				name: 'inventoryOutReport',
				component: () => import('@/views/outSourcing/inventoryOutReport/index.vue')
			},
		]
	}
];
