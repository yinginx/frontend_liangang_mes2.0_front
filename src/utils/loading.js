// loading.js  
import { ElLoading } from 'element-plus';

let loadingInstance = null;

export function startLoading() {
    if (!loadingInstance) {
        loadingInstance = ElLoading.service({ fullscreen: true });
    }
}

export function stopLoading() {
    if (loadingInstance) {
        loadingInstance.close();
        loadingInstance = null;
    }
}