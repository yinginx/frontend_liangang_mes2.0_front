import { materialPage, routeAdd, routeDel, routePage, routeUpdate,routeCopy } from '@/api/baseData/processRoute';
import type { FormInstance, FormRules } from 'element-plus';
import { ElMessage } from 'element-plus';
import { onMounted, ref } from 'vue';

interface productRecord {
	extension?: Record<string, any>;
	id: string;
	createTime?: string;
	updateUser?: string;
	updateTime?: string;
	createUser?: string;
	processName?: string;
	productId?: string;
	processPublished?: string;
	category?: string;
	description?: string;
	activate?: boolean;
	isDeleted?: boolean;
	clazz?: string;
}

interface routePageResult {
	records: productRecord[];
	total: number;
	size: number;
	current: number;
	optimizeCountSql: boolean;
	searchCount: boolean;
	pageSize: number;
}

interface ApiResponse<T> {
	code: number;
	message: string;
	data: T;
}

interface PaginationParams {
	current: number;
	pageSize: number;
}

interface FormData {
	processName: string;
	productId: string;
	processPublished: string;
}

interface materialData {
	id: string;
	materialName: string;
}

export function useproductData() {
	const searchParam = ref({});
	const isLoading = ref(true);
	const searchFields = ref([
		{
			name: 'processName',
			label: '工艺路线',
			component: 'input',
			placeholder: '',
			class: '!w-[350px]'
		}
	]);
	const pageTotal = ref(0);
	const paginatedData = ref<productRecord[]>([]);
	const paginationParams = ref<PaginationParams>({
		current: 1,
		pageSize: 10
	});
	const formData = ref<FormData>({
		processName: '',
		productId: '',
		processPublished: ''
	});

	// 表单实例引用
	const ruleFormRef = ref<FormInstance>();

	// 材料列表
	const materialList = ref<materialData[]>([]);

	// 验证规则
	const rules = ref<FormRules>({
		processName: [{ required: true, message: '请输入工艺路线', trigger: 'blur' }],
		productId: [{ required: true, message: '请输入产品名称', trigger: 'blur' }],
		processPublished: [{ required: true, message: '请选择是否发布', trigger: 'blur' }],
		property: [{ required: true, message: '请选择属性', trigger: 'blur' }]
	});
	const fetchPageData = async (params: any) => {
		isLoading.value = true;
		try {
			const response = (await routePage({
				...params,
				current: paginationParams.value.current,
				pageSize: paginationParams.value.pageSize
			})) as unknown as ApiResponse<routePageResult>;

			if (response.code === 200) {
				const { records, total } = response.data;
				paginatedData.value = records.map((item) => {
					return {
						...item.extension,
						...item
					};
				});
				pageTotal.value = total;
			} else {
				ElMessage.error(response.message);
			}
		} catch (error) {
			ElMessage.error('数据获取失败');
		} finally {
			isLoading.value = false; // 数据加载完成后设置为 false
		}
	};

	onMounted(() => {
		fetchPageData(searchParam.value);
		fetchLineData();
	});
	const fetchLineData = () => {
		materialPage({}).then((res) => {
			materialList.value = res.data;
		});
	};
	const handleSearch = () => {
		fetchPageData(searchParam.value);
	};

	const handleSizeChange = (val: number) => {
		paginationParams.value.pageSize = val;
		fetchPageData(searchParam.value);
	};

	const handleCurrentChange = (val: number) => {
		paginationParams.value.current = val;
		fetchPageData(searchParam.value);
	};

	const addproduct = async (data: Partial<productRecord>, callback: () => void) => {
		if (ruleFormRef.value) {
			await ruleFormRef.value.validate(async (valid) => {
				if (valid) {

					if(data?.copy){
						routeCopy({
							id:data.id,
							processName:data.processName,
						}).then(res=>{
							if(res.code===200){
								ElMessage.success('复用成功');
								fetchPageData(searchParam.value);
						callback();
							}
						})

						return
					}
					const url = data?.id ? routeUpdate : routeAdd;
					const response = (await url(data)) as unknown as ApiResponse<void>;
					if (response.code === 200) {
						if (data?.id) {
							ElMessage.success('更新成功');
						} else {
							ElMessage.success('添加成功');
						}
						fetchPageData(searchParam.value);
						callback();
					} else {
						ElMessage.error(response.message);
					}
				}
			});
		}
	};

	const deleteproduct = async (id: string) => {
		try {
			const response = (await routeDel({
				ids: [id]
			})) as unknown as ApiResponse<void>;
			if (response.code === 200) {
				ElMessage.success('删除成功');
				fetchPageData(searchParam.value);
			} else {
				ElMessage.error(response.message);
			}
		} catch (error) {
			ElMessage.error('删除失败');
		}
	};

	return {
		searchParam,
		pageTotal,
		paginatedData,
		searchFields,
		isLoading,
		handleSearch,
		handleSizeChange,
		handleCurrentChange,
		addproduct,
		deleteproduct,
		paginationParams,
		ruleFormRef,
		rules,
		formData,
		materialList
	};
}
