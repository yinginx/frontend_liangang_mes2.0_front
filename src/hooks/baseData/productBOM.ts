import { MaterialBomAdd, MaterialBomDel, MaterialBomPage, MaterialBomUpdate, productAll } from "@/api/baseData/productBOM"
import type { FormInstance, FormRules } from "element-plus"
import { ElMessage } from "element-plus"
import { onMounted, ref } from "vue"

interface toolRecord {
  extension?: Record<string, any>
  id: string
  createTime?: string
  updateUser?: string
  updateTime?: string
  createUser?: string
  materialBomName: string
  materialBomVersion: string
  productId?: string
  description?: string
  activate?: boolean
  isDeleted?: boolean
  clazz?: string
}

interface MaterialBomPageResult {
  records: toolRecord[]
  total: number
  size: number
  current: number
  optimizeCountSql: boolean
  searchCount: boolean
  pageSize: number
}

interface ApiResponse<T> {
  code: number
  message: string
  data: T
}

interface PaginationParams {
  current: number
  pageSize: number
}

interface FormData {
  materialBomName: string
  materialBomVersion: string
  productId: string
}

export function useToolData() {
  const searchParam = ref({})
  const isLoading = ref(true)
  const searchFields = ref([
    {
      name: "materialBomName",
      label: "名称",
      component: "input",
      placeholder: "",
      class: "!w-[350px]",
    },
    {
      name: "materialBomVersion",
      label: "版本",
      component: "input",
      placeholder: "",
      class: "!w-[350px]",
    },
  ])
  const pageTotal = ref(0)
  const paginatedData = ref<toolRecord[]>([])
  const productData = ref([])
  const paginationParams = ref<PaginationParams>({ current: 1, pageSize: 10 })
  const formData = ref<FormData>({
    materialBomName: "",
    materialBomVersion: "",
    productId: "",
  })

  const ruleFormRef = ref<FormInstance>() // 表单实例引用

  const rules = ref<FormRules<FormData>>({
    materialBomName: [{ required: true, message: "请输入物料bom编码", trigger: "blur" }],
    materialBomVersion: [{ required: true, message: "请输入物料bom版本", trigger: "blur" }],
    productId: [{ required: true, message: "请输入产品名称", trigger: "blur" }],
  })
  const fetchPageData = async (params: any) => {
    isLoading.value = true
    try {
      const response = (await MaterialBomPage({
        ...params,
        current: paginationParams.value.current,
        pageSize: paginationParams.value.pageSize,
      })) as unknown as ApiResponse<MaterialBomPageResult>

      if (response.code === 200) {
        const { records, total } = response.data
        paginatedData.value = records.map((item) => {
          return {
            ...item.extension,
            ...item,
          }
        })
        pageTotal.value = total
      } else {
        ElMessage.error(response.message)
      }
    } catch (error) {
      ElMessage.error("数据获取失败")
    } finally {
      isLoading.value = false // 数据加载完成后设置为 false
    }
  }

  onMounted(() => {
    fetchPageData(searchParam.value)
    fetchLineData()
  })

  const fetchLineData = () => {
    productAll({}).then((res) => {
      productData.value = res.data
    })
  }
  const handleSearch = () => {
    fetchPageData(searchParam.value)
  }

  const handleSizeChange = (val: number) => {
    paginationParams.value.pageSize = val
    fetchPageData(searchParam.value)
  }

  const handleCurrentChange = (val: number) => {
    paginationParams.value.current = val
    fetchPageData(searchParam.value)
  }

  const addTool = async (data: Partial<toolRecord>, callback: () => void) => {
    if (ruleFormRef.value) {
      await ruleFormRef.value.validate(async (valid) => {
        if (valid) {
          const url = data?.id ? MaterialBomUpdate : MaterialBomAdd
          const response = (await url(data)) as unknown as ApiResponse<void>
          if (response.code === 200) {
            if (data?.id) {
              ElMessage.success("更新成功")
            } else {
              ElMessage.success("添加成功")
            }
            fetchPageData(searchParam.value)
            callback()
          } else {
            ElMessage.error(response.message)
          }
        }
      })
    }
  }

  const deleteTool = async (id: string) => {
    try {
      const response = (await MaterialBomDel({ ids: [id] })) as unknown as ApiResponse<void>
      if (response.code === 200) {
        ElMessage.success("删除成功")
        fetchPageData(searchParam.value)
      } else {
        ElMessage.error(response.message)
      }
    } catch (error) {
      ElMessage.error("删除失败")
    }
  }

  return {
    searchParam,
    pageTotal,
    paginatedData,
    searchFields,
    isLoading,
    handleSearch,
    handleSizeChange,
    handleCurrentChange,
    addTool,
    deleteTool,
    paginationParams,
    ruleFormRef,
    rules,
    formData,
    productData,
  }
}

export function useProductBOMData() {
  const currentItem = ref({})
  const drawerVisible = ref(false)
  const drawerBOMData = ref([])
  const handleBOMAssociation = (data) => {
    console.log(data, "sss")
    currentItem.value = data
    drawerVisible.value = true
    drawerBOMData.value = [{}, {}]
  }
  return {
    currentItem,
    drawerVisible,
    drawerBOMData,
    handleBOMAssociation,
  }
}
