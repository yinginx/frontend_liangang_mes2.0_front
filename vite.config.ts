import { fileURLToPath, URL } from 'node:url';

import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import { defineConfig, loadEnv } from 'vite';
// import vueDevTools from 'vite-plugin-vue-devtools'

// vite.config.ts
import AutoImport from 'unplugin-auto-import/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import Components from 'unplugin-vue-components/vite';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
	const env = loadEnv(mode, process.cwd());
	return {
		plugins: [
			vue(),
			vueJsx(),
			AutoImport({
				imports: ['vue', 'vue-router'],
				eslintrc: {
					enabled: false, // Default `false`
					filepath: './.eslintrc-auto-import.json',
					globalsPropValue: true
				},
				dts: 'src/auto-import.d.ts'
			}),
			Components({
				resolvers: [ElementPlusResolver()]
			})
		],
		resolve: {
			alias: {
				'@': fileURLToPath(new URL('./src', import.meta.url))
			}
		},
		server: {
			proxy: {
				'/api': {
					target: env.VITE_API_PROXY_TARGET,
					changeOrigin: true
					// rewrite: path => path.replace(/^\/api/, '/api')`
				},
				'/webroot': {
					target: 'http://121.228.40.79:1666',
					changeOrigin: true
				},
				'/job': {
					target: 'http://172.30.190.30:8080',
					changeOrigin: true
					// rewrite: path => path.replace(/^\/job/, '/job')`
				}
			}
		}
	};
});
