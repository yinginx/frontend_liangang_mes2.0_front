// env.d.ts
interface ImportMetaEnv {
	// 其他环境变量...
	VITE_API_PROXY_TARGET?: string; // 使用 ? 表示这个属性是可选的

	// NODE_TLS_REJECT_UNAUTHORIZED: 0;

}

interface ImportMeta {
	readonly env: ImportMetaEnv;
}
